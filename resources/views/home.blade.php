@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div> 
            <div class="buttonstyle col-md-6 col-md-offset-3">
                    <a href=" {{route('doctors.index')}} "> 
                <button class="btn btn-success btn-lg btn-block"> Continue </button></a>
            </div>
        </div>
    </div>
</div>
@endsection
