<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    
    <!-- Core CSS - Include with every page -->
    <link href="/css/bootstrap.css" rel="stylesheet" />
	<link href="/css/font-awesome.css" rel="stylesheet" />
   	{{-- <link href="/css/style.css" rel="stylesheet" /> --}}
   	{{-- <link href="/css/main-style.css" rel="stylesheet" /> --}}

<style>
		#company_name {padding-top: 25px;text-align: center;padding-bottom: 25px;font-size: 22px;}
		.mr {margin-right: 20px;}
		.fa-fw {text-align: center;}
		ul {list-style-type: none;margin:0;padding: 0;font-size: 16px;color: #fff; }
		li {border-top: 1px solid white;}
		img {margin: auto;display: table-caption;border-radius: 50%;background: #fff;width: 60px;height: 60px;padding: 5px;}
	
			 /* The side navigation menu */
		.sidenav {
		    height: 100%; /* 100% Full-height */
		    width: 0; /* 0 width - change this with JavaScript */
		    position: fixed; /* Stay in place */
		    z-index: 1; /* Stay on top */
		    top: 0;
		    left: 0;
		    background-color: #235eaa; /* Black*/
		    overflow-x: hidden; /* Disable horizontal scroll */
		    /*padding-top: 60px; /* Place content 60px from the top */*/
		    transition: 1.0s; /* 0.5 second transition effect to slide in the sidenav */
		}

		/* The navigation menu links */
		.sidenav a {
		    padding: 20px 15px;
		    /*text-decoration: none;
		    font-size: 25px;*/
		    color: #fff;
		    display: block;
		    transition: 0.3s
		}

		/* When you mouse over the navigation links, change their color */
		.sidenav a:hover, .offcanvas, a:focus{
		    background-color: #1a467f;
		}

		.active {background-color: #1a467f;}

		/* Position and style the close button (top right corner) */
		.sidenav .closebtn {
		    font-size: 36px;
		}

		/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
		#main {
		    transition: margin-left .5s;
		    padding: 20px;
		}

		/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
		@media screen and (max-height: 450px) {
		    .sidenav {padding-top: 15px;}
		    .sidenav a {font-size: 18px;}
		} 
	</style>
</head>