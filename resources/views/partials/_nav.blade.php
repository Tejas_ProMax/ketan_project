 <div id="mySidenav" class="sidenav">
    
    <div style="padding-bottom: 35px;"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="float: right;">&times;</a></div>
    <div>
        <ul>
            <li style="border: none;">
                <div>
                    <div>
                        <img src="/images/user.jpg" alt="logo">
                    </div>
                    <div>
                        <div id="company_name">Spandan Hospital</div>      
                    </div>
                </div>
            </li>
            <li class="{{ Request::is('/patients') ? "active" : "" }}">
                <a href=" {{route('patients.index')}} ">
                    <i class="fa fa-home fa-fw mr"></i> Home
                </a>
            </li> 
               
            <li>
                <a href=" {{route('doctors.index')}} ">
                    <i class="fa fa-user-md fa-fw mr"></i> Add Doctors
                </a>
            </li>

            <li>
                <a href=" {{route('tokens.index')}} "><i class="fa fa-edit fa-fw mr"></i>Add Tokens</a>
            </li>
            <li style="border-bottom: 1px solid white;">
                <a href=" {{route('specialities.index')}} "> Add Speciality </a>
            </li>
        </ul>
    </div>
</div>

<!-- Use any element to open the sidenav -->
<div style="margin-left: 15px;margin-top: 25px;">
    <span onclick="openNav()" style="color: red;font-size: 22px;">&#9776; open</span>
</div>
















































{{-- <nav class="navbar-default navbar-static-side" role="navigation">
    
    <!-- sidebar-collapse -->
    <div class="sidebar-collapse">
        
        <!-- side-menu -->
        <ul class="nav" id="side-menu">
            <li>
                
                <!-- user image section-->
                <div class="user-section">
                    <div class="user-section-inner">
                        <img src="assets/img/user.jpg" alt="">
                    </div>
                    <div class="user-info">
                        <div>Spandan Hospital</div>      
                    </div>
                </div>  end user image section
            </li>
                            
            <li class="selected">
                <a href="panel.html">
                    <i class="fa fa-home fa-fw mr"></i> Home
                </a>
            </li> 
           
            <li>
                <a href="add_doctors.html">
                    <i class="fa fa-user-md fa-fw mr"></i> Add Doctors
                </a>
            </li>

            <li>
                <a href="add_tokens.html"><i class="fa fa-edit fa-fw mr"></i>Add Tokens</a>
            </li>
        </ul>   <!-- end side-menu -->
    </div>  <!-- end sidebar-collapse -->
</nav>  <!-- end navbar side -->
 --}}