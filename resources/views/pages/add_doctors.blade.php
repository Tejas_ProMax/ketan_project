@extends('main')

@section('title','Admin | Add Doctors')

@section('content')
<style>
	.button {color: #ffffff;float: right;padding: 6px 8px;}
	td {vertical-align: middle;}
</style>
<div id="main">
	<div class="row"> {{-- Buttons (Modal) --}}
	    <div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
	        <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#add-doctor">
	        	<i class="fa fa-user-md"></i> Add Doctor
	        </button>
	        <div class="modal fade" id="add-doctor" role="dialog">
		      	<div class="modal-dialog">
		            <div class="modal-content">
		            	<div class="modal-header">
		            		<button type="button" class="close" data-dismiss="modal"></button>
		                	<h1 class="modal-title">Add Doctor</h1><br>
		                </div>
		                <div class="modal-body">

		              		<form method="POST" action=" {{route('doctors.store')}} " id="doctormodal">
		              			{{csrf_field()}}
			                	<div class="form-group">
			                    	<label for="fname" class="control-label">First Name</label>
			                    	<input type="text" name="first_name" placeholder="First name" class="form-control" id="fname">
			                	</div>

			                	<div class="form-group">
			                    	<label for="lname" class="control-label">Last Name</label>
			                    	<input type="text" name="last_name" placeholder="Last name" class="form-control" id="lname">
			                	</div>

		                        <div class="form-group">
		                          	<label for="speciality"> Speciality (Select One) </label>
		                          	<select name="speciality" class="form-control" id="speciality" form="doctormodal">
		                          		@foreach($specialities as $speciality)
		                          			<option value="{{$speciality->id}}">{{$speciality->name}}</option>
		                          		@endforeach
		                          	</select>
	                        	</div> 
			                	<div style="text-align: center;">
			                		<button class="btn btn-success btn-lg" type="submit" style="padding-left: 40px;padding-right: 40px;margin-top: 20px;">Save</button>
			                	</div>
							</form>
						</div>
		              	<div class="modal-footer">
		                    <button class="btn btn-danger" data-dismiss="modal" type="reset">Close</button>
		             	</div>
		            </div>
		        </div>
		    </div>
	    </div>
	</div>
	
	<div class="row">
		<div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
			<div class="table-responsive" style="margin-top: 40px;">
				<table class="table" style="font-size: 16px;">
					<tr>
						<th>Sr No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Speciality</th>
					</tr>
					@foreach($doctors as $doctor)
						<tr>
							<td> {{$loop->index+1}} </td>
							<td> {{$doctor->first_name}} </td>
							<td> {{$doctor->last_name}} </td>
							<td> {{$doctor->speciality->name}} </td>
							<td> <a href="" class="button" style="background-color: red;"> <i class="fa fa-trash-o"> </i> </a> </td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>


@stop