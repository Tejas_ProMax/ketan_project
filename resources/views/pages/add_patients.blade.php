@extends('main')

@section('title','Admin | Add Patients')

@section('content')
<style>
	.button {color: #ffffff;padding: 6px 8px;}
	td {vertical-align: middle;text-align: center;}
	th {text-align: center;
</style>
<div id="main">
	<div class="row"> {{-- Buttons (Modal) --}}
	    <div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
	        <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#add-patient">
	        	<i class="fa fa-user-md"></i> Add Patient
	        </button>
	        <div class="modal fade" id="add-patient" role="dialog">
		      	<div class="modal-dialog">
		            <div class="modal-content">
		            	<div class="modal-header">
		            		<button type="button" class="close" data-dismiss="modal"></button>
		                	<h1 class="modal-title">Add Patient</h1><br>
		                </div>
		                <div class="modal-body">

		              		<form method="POST" action=" {{route('patients.store')}} " id="patientmodal">
		              			{{csrf_field()}}
			                	<div class="form-group">
			                    	<label for="fname" class="control-label">First Name</label>
			                    	<input type="text" name="first_name" placeholder="First name" class="form-control" id="fname">
			                	</div>

			                	<div class="form-group">
			                    	<label for="lname" class="control-label">Last Name</label>
			                    	<input type="text" name="last_name" placeholder="Last name" class="form-control" id="lname">
			                	</div>

		                        <div class="form-group">
		                          	<label for="token"> Token No. (Select One) </label>
		                          	<select name="token" class="form-control" id="token" form="patientmodal">
		                          		@foreach($tokens as $token)
                     			<option value="{{$token->id}}">{{$token->token_number}}</option>
		                          		@endforeach
		                          	</select>
	                        	</div> 
	                        	<div class="form-group">
		                          	<label for="doctor"> Doctor (Select One) </label>
		                          	<select name="doctor" class="form-control" id="doctor" form="patientmodal">
		                          		@foreach($doctors as $doctor)
		                          			<option value="{{$doctor->id}}">{{$doctor->first_name. " " .$doctor->last_name}}</option>
		                          		@endforeach
		                          	</select>
	                        	</div> 
			                	<div style="text-align: center;">
			                		<button class="btn btn-success btn-lg" type="submit" style="padding-left: 40px;padding-right: 40px;margin-top: 20px;">Save</button>
			                	</div>
							</form>
						</div>
		              	<div class="modal-footer">
		                    <button class="btn btn-danger" data-dismiss="modal" type="reset">Close</button>
		             	</div>
		            </div>
		        </div>
		    </div>
	    </div>
	</div>
	
	<div class="row">
		<div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
			<div class="table-responsive" style="margin-top: 40px;">
				<table class="table" style="font-size: 16px;">
					<tr>
						<th>Sr No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Token No.</th>
						<th>Doctor's Name</th>
						<th></th>
						<align: center;<
						th {text-align: center;/th>
					</tr>
					@foreach($patients as $patient)
						<tr>
							<td> {{$loop->index+1}} </td>
							<td> {{$patient->first_name}} </td>
							<td> {{$patient->last_name}} </td>
							<td> {{$patient->token->token_number}} </td>
							<td> {{$patient->doctor[0]->first_name. " " .$patient->doctor[0]->last_name}} </td>
							<td> <a href="" class="button" style="background-color:green; "> <i class="fa fa-phone"> </i> </a> </td>
							<td> 
								<form action="{{route('patients.destroy',$patient->id)}}" method="POST">
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button type="submit" class="fa fa-trash-o button" style="background-color: red;border: none;">
									</button>
								</form>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>


@stop