@extends('main')

@section('title','Admin | Add Speciality')

@section('content')

<div id="main">
	<div class="row">
		<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
			<button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#add-speciality">
		        <i class="fa fa-plus"></i> Add Speciality
		    </button>
	        <div class="modal fade" id="add-speciality" role="dialog">
		      	<div class="modal-dialog">
		            <div class="modal-content">
		            	<div class="modal-header">
		            		<button type="button" class="close" data-dismiss="modal"></button>
		                	<h1 class="modal-title">Add Speciality</h1><br>
		                </div>
		                <div class="modal-body">

		              		<form method="POST" action=" {{route('specialities.store')}} ">
		              			{{csrf_field()}}
			                	<div class="form-group">
			                    	<label for="speciality" class="control-label"> Speciality </label>
			                    	<input type="text" name="speciality" placeholder="Enter the Speciality you want to save" class="form-control" id="speciality">
			                	</div>
			                	<div style="text-align: center;">
			                		<button class="btn btn-success btn-lg" type="submit" style="padding-left: 40px;padding-right: 40px;margin-top: 20px;">Save</button>
			                	</div>
							</form>
						</div>
		              	<div class="modal-footer">
		                    <button class="btn btn-danger" data-dismiss="modal" type="reset">Close</button>
		             	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
			<div class="table-responsive" style="margin-top: 40px;">
				<table class="table">
					<tr>
						<th>Sr No.</th>
						<th>Speciality</th>
					</tr>
					@foreach($specialities as $speciality)
						<tr>
							<td> {{$loop->index+1}} </td>>
							<td> {{$speciality->name}} </td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>

@stop