@extends('main')

@section('title','Admin | Add Token')

@section('content')

<style>
	.button {color: #ffffff;padding: 6px 8px;}
	td {vertical-align: middle;text-align: center;}
	th {text-align: center;}
</style>
<div id="main">
	<div class="row"> {{-- Buttons (Modal) --}}
	    <div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-12">
	        <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#add-token">
	        	<i class="fa fa-user-md"></i> Add Token
	        </button>
	        <div class="modal fade" id="add-token" role="dialog">
		      	<div class="modal-dialog">
		            <div class="modal-content">
		            	<div class="modal-header">
		            		<button type="button" class="close" data-dismiss="modal"></button>
		                	<h1 class="modal-title">Add Token</h1><br>
		                </div>
		                <div class="modal-body">

		              		<form method="POST" action=" {{route('tokens.store')}} " id="tokenmodal">
		              			{{csrf_field()}}

			                	<div class="alert-danger"><p>Enter the range of the token numbers to be created</p></div>
			                	<div class="form-group">
			                    	<label for="token_start" class="control-label">Start No.</label>
			                    	<input type="number" name="token_start" placeholder="Start Token No." class="form-control" id="token_start">
			                	</div>

								<div class="form-group">
			                    	<label for="token_end" class="control-label">End No.</label>
			                    	<input type="number" name="token_end" placeholder="End Token No." class="form-control" id="token_end">
			                	</div>


			                	<div style="text-align: center;">
			                		<button class="btn btn-success btn-lg" type="submit" style="padding-left: 40px;padding-right: 40px;margin-top: 20px;">Save</button>
			                	</div>
							</form>
						</div>
		              	<div class="modal-footer">
		                    <button class="btn btn-danger" data-dismiss="modal" type="reset">Close</button>
		             	</div>
		            </div>
		        </div>
		    </div>
	    </div>
	</div>
	
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
			<div class="table-responsive" style="margin-top: 40px;">
				<table class="table table-bordered">
					<tr>
						<th>Sr No.</th>
						<th>Token No.</th>
						<th></th>
					</tr>
					@foreach($tokens as $token)
						<tr>
							<td> {{$loop->index+1}} </td>
							<td> {{$token->token_number}} </td>
							<td> 
								<form action="{{route('tokens.destroy',$token->id)}}" method="POST">
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button type="submit" class="fa fa-trash-o button" style="background-color: red;border: none;">
									</button>
								</form>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>

@stop