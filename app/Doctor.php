<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
	public function speciality()
	{
		return $this->belongsTo('App\Speciality');
	}

    public function patient()
        {
            return $this->belongsToMany('App\Patient','doctor_patient')->withTimestamps();
        }    

    public function index()
    {
    	$doc = $this;
    	$result = $doc->orderBy('id')->get();
    	return $result;
    }


}
