<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    public function patient()
    {
    	return $this->hasOne('App\Patient');
    }

    public function index()
    {
    	$token = $this;
    	$result = $token->orderBy('id')->get();
    	return $result;
    }
}
