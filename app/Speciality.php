<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    public function doctor()
    {
        return $this->hasMany('App\Doctor');
    }

    public function index()
    {
        $spe = $this;
        $result = $spe->orderBy('id')->get();
        return $result;
    }
    public function find_speciality($speciality = null)
    {
    	if(is_null($speciality))
    	{
    		throw new Exception("Null Value Given", 1);    		
    	}

    	try 
    	{
    		$spe = $this;
    		$result = $spe->where('speciality',$speciality)->get('id')->first();
    		return $result;
    	} 
    	catch (Exception $e) 
    	{
    		throw new Exception("Incorrect Parameter Value", 1);
    		
    	}
    }
}
