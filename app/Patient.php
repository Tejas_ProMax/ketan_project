<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public function token()
    {
    	return $this->belongsTo('App\Token');
    }

    public function doctor()
    {
    	return $this->belongsToMany('App\Doctor','doctor_patient')->withTimestamps();
    }

    public function index()
    {
    	$patobj = $this;
    	$result =  $patobj->orderBy('id')->get();
    	return $result;
    }
}
