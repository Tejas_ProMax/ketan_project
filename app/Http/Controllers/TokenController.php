<?php

namespace App\Http\Controllers;

use Session;
use App\Token;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tokenobj = new Token();

        $tokens = $tokenobj->index();

        return view('pages.add_tokens')->withTokens($tokens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['token_start'=>'required|integer','token_end'=>'required|integer']);


        $start = (int)$request->token_start;
        $end = $request->token_end;

        $j = 0;
        for ($i = $start; $i <= $end ; $i++) 
        {     
            $tokenobj = new Token();
            $tokenobj->token_number = $i;
            $tokenobj->save();
        }

        Session::flash("Success","Token were saved successfully.");

        return redirect()->route('tokens.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tokenobj = Token::find($id);

        $tokenobj -> delete();

        Session::flash('success','The token was deleted successfully!');

        return redirect() -> route('tokens.index');
    }
}
