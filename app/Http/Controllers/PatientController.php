<?php

namespace App\Http\Controllers;

use Session;
use App\Token;
use App\Doctor;
use App\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patobj = new Patient();
        $patients = $patobj->index();
        $tokenobj = new Token();
        $tokens = $tokenobj->index();
        $docobj = new Doctor();
        $doctors = $docobj->index();
        return view('pages.add_patients')->withPatients($patients)->withTokens($tokens)->withDoctors($doctors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['first_name'=>'required|alpha|max:60|min:1','last_name'=>'required|alpha|max:60|min:1','token'=>'required|integer','doctor'=>'required|integer']);

        $patobj = new Patient();

        $patobj->first_name = $request->first_name;
        $patobj->last_name = $request->last_name;
        $patobj->token_id = $request->token;

        $patobj->save();

        $patobj->doctor()->attach($request->doctor);

        Session::flash("success","The Patient was saved successfully :)");

        return redirect()->route('patients.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patobj = Patient::find($id);

        $patobj->doctor()->detach($patobj->doctor[0]->id);

        $patobj -> delete();

        Session::flash('success','The patient was deleted successfully!');

        return redirect() -> route('patients.index');
    }
}
