<?php

namespace App\Http\Controllers;

use Session;
use App\Doctor;
use App\Speciality;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docobj = new Doctor();
        $doctors = $docobj->index();
        $speobj = new Speciality();
        $specialities = $speobj->index();
        return view('pages.add_doctors')->withSpecialities($specialities)->withDoctors($doctors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,['first_name'=>'required|alpha|max:60','last_name'=>'required|alpha|max:60','speciality'=>'required|integer']);

        $docobj = new Doctor();
        // $specialobj = new Speciality();
        // $result = $specialobj->find_speciality($request->speciality);

        $docobj->first_name = $request->first_name;
        $docobj->last_name = $request->last_name;
        $docobj->speciality_id = $request->speciality;

        $docobj->save();

        Session::flash("success","Doctor was saved successfully :)");

        return redirect()->route('doctors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
